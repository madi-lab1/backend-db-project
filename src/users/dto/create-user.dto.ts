import { IsNotEmpty, Length, Matches } from 'class-validator';
export class CreateUserDto {
  @IsNotEmpty()
  @Length(4, 16)
  login: string;

  @IsNotEmpty()
  @Length(5, 16)
  name: string;

  @Matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@$!%*#?&^_-]).{8,}$/)
  @Length(8, 16)
  @IsNotEmpty()
  password: string;
}
