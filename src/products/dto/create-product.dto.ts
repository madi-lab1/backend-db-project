import { IsNotEmpty, MinLength, Min } from 'class-validator';

export class CreateProductDto {
  @IsNotEmpty()
  @MinLength(5)
  name: string;

  @IsNotEmpty()
  @Min(0)
  price: number;
}
