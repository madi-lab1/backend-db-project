import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private usersRepository: Repository<Product>,
  ) {}
  create(createProductDto: CreateProductDto): Promise<Product> {
    const product: Product = new Product();
    product.name = createProductDto.name;
    product.price = createProductDto.price;
    return this.usersRepository.save(product);
  }

  findAll(): Promise<Product[]> {
    return this.usersRepository.find();
  }

  findOne(id: number): Promise<Product> {
    return this.usersRepository.findOneBy({ id: id });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.usersRepository.findOneBy({ id: id });
    const updatedProduct = { ...product, ...updateProductDto };
    return this.usersRepository.save(updatedProduct);
  }

  async remove(id: number) {
    const product = await this.usersRepository.findOneBy({ id: id });
    return this.usersRepository.remove(product);
  }
}
